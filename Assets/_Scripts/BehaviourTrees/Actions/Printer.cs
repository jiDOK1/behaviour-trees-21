using UnityEngine;

public class Printer : Task
{
    public string Message = "Hello";

    public Printer(string message)
    {
        Message = message;
    }

    public override void OnStart()
    {
        Blackboard.Strings.TryGetValue("message for printer", out Message);
        Debug.Log($"OnStart: {Message}");
    }

    public override void OnStop()
    {
        Debug.Log($"OnStop: {Message}");
    }

    protected override BTStatus OnUpdate()
    {
        Debug.Log($"Update: {Message}");
        return BTStatus.Success;
    }
}
