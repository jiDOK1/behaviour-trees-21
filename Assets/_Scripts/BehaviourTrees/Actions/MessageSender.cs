using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageSender : Task
{
    public override void OnStart()
    {
        string message = "Mir f�llt nichts ein";
        if(!Blackboard.Strings.ContainsKey("message for printer"))
        {
            Blackboard.Strings.Add("message for printer", message);
        }
    }

    public override void OnStop()
    {
    }

    protected override BTStatus OnUpdate()
    {
        return BTStatus.Success;
    }
}
