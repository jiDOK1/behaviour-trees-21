using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTree : ScriptableObject
{
    Task root;

    BTStatus Update()
    {
        return root.Update();
    }
}
