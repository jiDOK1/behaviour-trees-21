public class RootTask : Task
{
    public Task Child;

    public RootTask(Task child)
    {
        Child = child;
    }

    public override void OnStart()
    {
        Child.OnStart();
    }

    public override void OnStop()
    {
        Child.OnStop();
    }

    protected override BTStatus OnUpdate()
    {
        return Child.Update();
    }
}
