using System.Collections.Generic;

public abstract class Composite : Task
{
    protected List<Task> children;

    public Composite(List<Task> children)
    {
        this.children = children;
    }
}
