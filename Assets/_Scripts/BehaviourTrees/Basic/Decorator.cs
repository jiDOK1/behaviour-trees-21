public abstract class Decorator : Task
{
    protected Task child;

    public Decorator(Task child)
    {
        this.child = child;
    }
}
