using UnityEngine;

public abstract class Task
{
    bool hasStarted;

    public abstract void OnStart();
    public abstract void OnStop();
    protected abstract BTStatus OnUpdate();

    public BTStatus Update()
    {
        if (!hasStarted)
        {
            hasStarted = true;
            OnStart();
        }
        BTStatus status = OnUpdate();
        if (status!=BTStatus.Running)
        {
            hasStarted = false;
            OnStop();
        }
        return status;
    }
}

public enum BTStatus
{
    Running, Failure, Success
}
