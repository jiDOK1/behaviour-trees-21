using System.Collections.Generic;
using UnityEngine;

public class BTTester : MonoBehaviour
{
    Task root;

    void Start()
    {
        MessageSender messageSender = new MessageSender();
        Printer printer1 = new Printer("Hello!");
        Printer printer2 = new Printer("World!");
        Printer printer3 = new Printer("I am a Printer");
        Wait wait1 = new Wait(printer1, 2f);
        Wait wait2 = new Wait(printer2, 2.5f);
        Wait wait3 = new Wait(printer3, 3f);
        //Selector selector = new Selector(new List<Task>() { wait1, wait2, wait3 });
        Sequence sequence = new Sequence(new List<Task>() { messageSender, wait1, wait2, wait3 });
        root = new RootTask(sequence);
        root.OnStart();
    }

    void Update()
    {
        root.Update();
    }
}
