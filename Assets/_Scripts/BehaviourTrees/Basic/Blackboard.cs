using System.Collections.Generic;
using UnityEngine;

public static class Blackboard
{
    public static Dictionary<string, int> Ints = new Dictionary<string, int>();
    public static Dictionary<string, float> Floats = new Dictionary<string, float>();
    public static Dictionary<string, Vector3> Vector3s = new Dictionary<string, Vector3>();
    public static Dictionary<string, string> Strings = new Dictionary<string, string>();
}
