using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : Composite
{
    int childIdx;

    public Selector(List<Task> children) : base(children)
    {
        this.children = children;
    }

    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    protected override BTStatus OnUpdate()
    {
        while (childIdx <= children.Count)
        {
            switch (children[childIdx].Update())
            {
                case BTStatus.Running:
                    return BTStatus.Running;
                case BTStatus.Failure:
                    if (++childIdx < children.Count)
                    {
                        continue;
                    }
                    else
                    {
                        childIdx = 0;
                        return BTStatus.Failure;
                    }
                case BTStatus.Success:
                    childIdx = 0;
                    return BTStatus.Success;
                default:
                    throw new System.Exception("unexpected return value from child");
            }
        }
        throw new System.Exception("unexpected outside loop");
    }
}
