using System.Collections.Generic;

public class RandomSelector : Selector
{
    public RandomSelector(List<Task> children) : base(children)
    {
        this.children = children;
    }

    public override void OnStart()
    {
        children = children.Shuffle();
    }
}
