using System.Collections.Generic;

public class Sequence : Composite
{
    int childIdx;

    public Sequence(List<Task> children) : base(children)
    {
        this.children = children;
    }

    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    protected override BTStatus OnUpdate()
    {
        while (childIdx <= children.Count)
        {
            switch (children[childIdx].Update())
            {
                case BTStatus.Running:
                    return BTStatus.Running;
                case BTStatus.Failure:
                    childIdx = 0;
                    return BTStatus.Failure;
                case BTStatus.Success:
                    if (++childIdx < children.Count)
                    {
                        continue;
                    }
                    else
                    {
                        childIdx = 0;
                        return BTStatus.Success;
                    }
                default:
                    throw new System.Exception("Sequencer: unexpected return value from child");
            }
        }
        throw new System.Exception("Sequencer: unexpected outside while loop!");
    }
}
