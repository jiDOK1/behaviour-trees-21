using UnityEngine;

public class Wait : Decorator
{
    float duration;
    float timer;

    public Wait(Task child, float duration) : base(child)
    {
        this.child = child;
        this.duration = duration;
    }

    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    protected override BTStatus OnUpdate()
    {
        timer += Time.deltaTime;
        if(timer < duration)
        {
            return BTStatus.Running;
        }
        else
        {
            switch (child.Update())
            {
                case BTStatus.Running:
                    return BTStatus.Running;
                case BTStatus.Failure:
                    timer = 0f;
                    return BTStatus.Failure;
                case BTStatus.Success:
                    timer = 0f;
                    return BTStatus.Success;
                default:
                    throw new System.Exception("unexpected return value from child");
            }
        }
    }
}
