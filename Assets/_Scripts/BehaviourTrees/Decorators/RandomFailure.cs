using UnityEngine;

public class RandomFailure : Decorator
{
    float probability;

    public RandomFailure(Task child, float probability) : base(child)
    {
        this.child = child;
        this.probability = probability;
    }

    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    protected override BTStatus OnUpdate()
    {
        switch (child.Update())
        {
            case BTStatus.Running:
                return BTStatus.Running;
            case BTStatus.Failure:
                return BTStatus.Failure;
            case BTStatus.Success:
                BTStatus status = Random.value < probability ? BTStatus.Failure : BTStatus.Success;
                return status;
            default:
                throw new System.Exception("unexpected return value from child!");
        }
    }
}
