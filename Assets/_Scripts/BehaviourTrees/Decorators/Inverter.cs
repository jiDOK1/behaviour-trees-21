public class Inverter : Decorator
{
    public Inverter(Task child) : base(child)
    {
        this.child = child;
    }

    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    protected override BTStatus OnUpdate()
    {
        switch (child.Update())
        {
            case BTStatus.Running:
                return BTStatus.Running;
            case BTStatus.Failure:
                return BTStatus.Success;
            case BTStatus.Success:
                return BTStatus.Failure;
            default:
                throw new System.Exception("unexpected return value from child");
        }
    }
}
