using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static List<T> Shuffle<T>(this List<T> ls)
    {
        System.Random rand = new System.Random();
        for (int curIdx = ls.Count - 1; curIdx >= 0; curIdx--)
        {
            int randIdx = rand.Next(curIdx);
            T tempChild = ls[randIdx];
            ls[randIdx] = ls[curIdx];
            ls[curIdx] = tempChild;
        }
        return ls;
    }
}
